import React, { useState } from 'react';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import Button from '@material-ui/core/Button';
import { Fingerprint } from '@material-ui/icons';
import TextareaAutosize from '@material-ui/core/TextareaAutosize';
import copy from 'copy-to-clipboard';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
const jwt = require('jsonwebtoken');

function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}

export default function Comp() {
  const [id, setId] = useState('s');
  const [cookieName, setCookieName] = useState('');
  const [token, setToken] = useState('Insert required fields to get a MoniTube token.');
  const [secret, setSecret] = useState('');
  const [open, setOpen] = useState(false);

  const useStyles = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'column',
      backgroundColor: '#8d91a2'
    },
    comp: {
      position: 'fixed',
      width: '11%',
      padding: 14
    },
    card: {
      minWidth: 213
    },
    button: {
      marginTop: '3%',
      marginLeft: '8%'
    },
    token: {
      marginTop: '3%',
      marginLeft: '8%'
    }
  }));
  const classes = useStyles();

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  const generateToken = () => {
    const token = jwt.sign({ userId: `${id}@idf.il` }, secret);
    const cookie = `document.cookie = "${cookieName}=${token}"`;
    copy(cookie);
    setToken(cookie);
  };
  return (
    <div className={classes.root}>
      <Card className={classes.card}>
        <div className={classes.comp}>
          <h3 style={{fontFamily: 'serif'}}>JWT cookie-generator</h3>
          <TextField required label="cookie-name" variant="filled" onChange={(e) => setCookieName(e.target.value)} />
          <TextField required label="secret" variant="filled" onChange={(e) => setSecret(e.target.value)} />
          <TextField
            required
            label="מס' אישי"
            variant="filled"
            InputProps={{
              endAdornment: <InputAdornment position="end">@idf.il</InputAdornment>
            }}
            onChange={(e) => setId(e.target.value)}
          />
          <Button
            onClick={() => {
              if (id !== '' && cookieName !== '' && secret !== '') {
                generateToken();
                handleClick();
              }
            }}
            variant="contained"
            color="primary"
            className={classes.button}
            endIcon={<Fingerprint />}
          >
            get a token
          </Button>

          <TextareaAutosize className={classes.token} rowsMax={4} aria-label="maximum height" value={token} />
          <Snackbar open={open} autoHideDuration={4000} onClose={handleClose}>
            <Alert onClose={handleClose} severity="success">
              Cookie has been copied to clipboard!
            </Alert>
          </Snackbar>
        </div>
      </Card>
    </div>
  );
}
